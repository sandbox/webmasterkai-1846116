<?php
/**
 * Our checkout panes.
 */
function uc_gift_certificate_checkout_pane() {
  $panes[] = array(
    'id' => 'certificate-discount',
    //'callback' => 'uc_gift_certificate_pane_gc_discount',
    'callback' => 'uc_gift_certificate_checkout_enter_code',
    'title' => t('Redeem a Gift Certificate'),
    'desc' => t('Allows shoppers to redeem a gift certificate code during checkout.'),
    'weight' => 5,
    'process' => TRUE,
  );
  $panes[] = array(
    'id' => 'gift_certificate',
    'callback' => 'uc_checkout_pane_gift_certificate',
    'title' => t('Gift Certificates'),
    'desc' => t('Allows shoppers to see that they have gift certificate credit(s) during checkout.'),
    'weight' => 5,
    'process' => TRUE,
  );

  return $panes;
}

/**
 * Checkout pane that displays how much in gift certificates a user has.
 */
function uc_checkout_pane_gift_certificate($op, &$order, $arg2) {
  global $user;

  switch ($op) {
    case 'settings':
      $form['uc_gift_certificate_pane_description'] = array(
        '#type' => 'textarea',
        '#title' => t('Checkout pane message'),
        '#default_value' => variable_get('uc_gift_certificate_pane_description', 'Enter a Gift Certificate code for this order.'),
      );

      $form['uc_gift_certificate_pane_msg'] = array(
        '#type' => 'textarea',
        '#title' => t('Checkout Pane Message'),
        '#default_value' => variable_get('uc_gift_certificate_pane_msg', uc_gift_certificate_get_setting_defaults('uc_gift_certificate_pane_msg')),
        '#description' => t('Gift certificate message in the checkout pane.'),
      );
      return $form;

    case 'view':
      // See if we are editing an order or creating it.
      $uid = empty($order->uid) ? $user->uid : $order->uid;
      if (!uc_gift_certificate_total($uid)){
        // Do not show the pane in this case.
        return;
      }

      $cert_arr = uc_gift_certificate_load_certificates($uid);
      $total = uc_gift_certificate_total($uid);
      $description = t('');
      $value = variable_get('uc_gift_certificate_pane_msg', uc_gift_certificate_get_setting_defaults('uc_gift_certificate_pane_msg'));
      $variables = array('!tot' => uc_currency_format($total));
      $value = strtr($value, $variables);
      $contents['gift_certificate_total'] = array('#value' => $value);
      return array('description' => $description, 'contents' => $contents);
  }
}

/*
 * Checkout pane that allows the entry of a gift certificate code.
 */
function uc_gift_certificate_pane_gc_discount($op, &$arg1, $arg2) {
  switch ($op) {
    case 'view':
      drupal_add_js(drupal_get_path('module', 'uc_gift_certificate') .'/uc_gift_certificate.js');
      $description = variable_get('uc_gift_certificate_pane_description', 'Enter a gift certificate code for this order.');
      $contents['certificate_code'] = array(
        '#type' => 'textfield',
        '#title' => t('Certificate Code'),
        '#default_value' => $arg1->data['gift_certificate'],
        '#size' => 25,
      );
      return array('description' => $description, 'contents' => $contents);
    case 'process':
      $arg1->data['certificate_code'] = $arg2['certificate_code'];
      return TRUE;
    case 'review':
      break;
  }
}

/**
 * Checkout Pane callback function for the 'enter a gift certificate code' textfield.
 *
 * Used to display a form in the checkout process so that customers
 * can enter discount coupons.
 *
 * @param string $op
 *   State of the action taking place. Switchable.
 * @param object $order
 *   The order that is being placed during checkout.
 * @param array $arg2
 *   This could be anything, but it probably contains the gift certificate code. :)
 */
function uc_gift_certificate_checkout_enter_code($op, &$order, $arg2) {
  global $user;
  $certificate_code = check_plain($arg2['certificate_code']);

  if ($user->mail) {
    $user_email = $user->mail;
  }
  elseif ($order->primary_email && $order->primary_email != NULL) {
    $user_email = $order->primary_email;
  }
  else {
    $user_email = NULL;
  }

  switch ($op) {
    case 'view':
      // If we just entered a new certificate at checkout, restrict it to this code.
      if (isset($order->uc_gift_certificate_added) && $order->uc_gift_certificate_added) {
        $certificate_added = array_shift(array_values($order->uc_gift_certificate_added));
      }

      $description = variable_get('uc_gift_certificate_pane_description', 'Enter a gift certificate code for this order.');
      $contents['certificate_code'] = array(
        '#type' => 'textfield',
        '#title' => t('Certificate Code'),
        '#default_value' => (isset($_SESSION['certificate_added'])) ? $_SESSION['certificate_added'] : '',
        '#size' => 25,
      );
      return array('description' => $description, 'contents' => $contents);

    case 'process':
      // User entered a code into the form textfield. Validate it, ignoring any certificates already
      // in the user's account.
      if ($certificate_code && $certificate = uc_gift_certificate_validate($certificate_code, $user_email)) {

        if ($certificate->valid == TRUE) {
          unset($order->uc_gift_certificate_added);

          // Pass the newly-added certificate into the $order object, for reference in $op = 'save'
          $order->uc_gift_certificate_added = $certificate;
          $_SESSION['certificate_added'] = $certificate_code;

          drupal_set_message(t("Successfully attached gift certificate !code to your order.", array('!code' => $certificate_code)));
        }
        else {
          drupal_set_message($certificate->error, 'error');
          return FALSE;
        }
      }
      break;
  }
}
